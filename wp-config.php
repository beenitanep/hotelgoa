<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'goapalace');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'FrPvi2;i7{>k__vOgnD4/k&!BEA-]XgD$3b)ThJGu)f!8>q;q~e#lY<3i)8z6[aw');
define('SECURE_AUTH_KEY',  'cqo[9x`&Q%aqD5@RoIodZ{o0y`cF6w8)}OS~}`tNpqs7fo^{uG?}^&e#(|(nxX#Q');
define('LOGGED_IN_KEY',    '(L6otq`>*B] ckN|Z:kF2I;u=|=lh]Z}1@yBX7MdBX29SwywnN`*S54d|`{lrFmO');
define('NONCE_KEY',        'Tiy/}r0Se.3h+{WQ[ri>7w|-8|-(o$J*4<khn%iaPx2_<Fog[ig[#]EUh9f;2bzw');
define('AUTH_SALT',        'xZ c}}F|UMwE9`#X;{1uJzK7u5n2C7FZ!o;:OYK|GoRZ|9sPw+riJhV3Nh@;Gq`:');
define('SECURE_AUTH_SALT', '?NO=m^3<1[ylc6)gO1c@%5}W8Hn/WOd*>Nv[+z_B9O,T*8@(h6a7yiwvTlBKp=_`');
define('LOGGED_IN_SALT',   'GPd<bP.#/Z[83A%>B-.b8^fq{M*tjy9HAr3]ZzNzLFa(Lq{n2vyEBTOK,(!]nc) ');
define('NONCE_SALT',       '2g:.y5DN%vD*0LOg{}w+`<>AJ;[$9Ap~J2|{@N(=1e$GGS;wGq;K~uAE}LZJ/PZ$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mh_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

<?php 
/*=======================================================================*/
// Wordpress Custom Functions
/*=======================================================================*/

/*=======================================================================*/
// Enqueue Scripts
/*=======================================================================*/


function wpdocs_theme_name_scripts() {
	wp_enqueue_style( 'bootstrap-min-css', get_template_directory_uri() . '/css/bootstrap.min.css',false,'1.1','all');
	wp_enqueue_style( 'flaticon', get_template_directory_uri() . '/css/flaticon.css',false,'1.1','all');
	wp_enqueue_style( 'font-awesome-min', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.min.css',false,'1.1','all');
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css',false,'1.1','all');
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.min.css',false,'1.1','all');
	wp_enqueue_style( 'bootstrap-multiselect', get_template_directory_uri() . '/css/bootstrap-multiselect.css',false,'1.1','all');
	wp_enqueue_style( 'jquery-ui', get_template_directory_uri() . '/css/jquery-ui.css',false,'1.1','all');
	wp_enqueue_style( 'daterangepicker', get_template_directory_uri() . '/css/daterangepicker.min.css',false,'1.1','all');
	wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css',false,'1.1','all');
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css',false,'1.1','all');
	wp_enqueue_script( 'jquery-3.1.0', get_template_directory_uri() . '/js/jquery-3.1.0.js', array(), '1.0.0', true );
	wp_enqueue_script( 'popper-min', get_template_directory_uri() . '/js/popper.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'bootstrap-min', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl.carousel.js', array(), '1.0.0', true );
	wp_enqueue_script( 'scrollPosStyler', get_template_directory_uri() . '/js/scrollPosStyler.js', array(), '1.0.0', true );
	wp_enqueue_script( 'bootstrap-multiselect', get_template_directory_uri() . '/js/bootstrap-multiselect.js', array(), '1.0.0', true );
	wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery-ui.js', array(), '1.0.0', true );
	wp_enqueue_script( 'moment-min', get_template_directory_uri() . '/js/moment.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'jquery-daterangepicker', get_template_directory_uri() . '/js/jquery.daterangepicker.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'jquery.magnific', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'isotope-pkgd', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array(), '1.0.0', true );

     
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

/*
	=====================================================
	Custom Menus
	=====================================================
*/
add_action( 'init', 'register_my_menus' );
function register_my_menus() {
	register_nav_menus(
		array(
			'primary'   => __('Primary Navigation'),
			'secondary' => __('Secondary Navigation')
		)
	);
} 

/*=======================================================================*/
// Including Functions Files
/*=======================================================================*/
require_once(TEMPLATEPATH . '/functions/cpt-slider.php');
require_once(TEMPLATEPATH . '/functions/cpt-service.php');
require_once(TEMPLATEPATH . '/functions/cpt-room-and-rates.php');
require_once(TEMPLATEPATH . '/functions/customfield.php');
require_once(TEMPLATEPATH . '/functions/cpt-testimonial.php');
require_once(TEMPLATEPATH . '/functions/cpt-gallery.php');

/*=======================================================================*/
// Image Thumbnails
/*=======================================================================*/
if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'slider', 675, 380,false );
	add_image_size( 'home-trek', 327, 176,false );
	add_image_size( 'home-about', 540, 304,false );
	add_image_size( 'home-service', 70, 70,false );
	add_image_size( 'home-room', 519, 346,false );
	add_image_size( 'room', 338, 190,false );
	add_image_size( 'room-details', 540, 360,false );
}

/*=======================================================================*/
// Image Thumbnails (add feature image) 
//post thumbnails enabled
/*=======================================================================*/
add_theme_support( 'post-thumbnails' );

/*=======================================================================*/
// Page excerpt support
/*=======================================================================*/

add_post_type_support('page', 'excerpt');

/*Allow .svg file upload*/

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


//Include Nav Walker class_alias to support bootsrap 4
    require( get_template_directory().'/bs4navwalker.php');
?>
<?php 
/*
Template Name: Gallery
*/
get_header();?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Gallery</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Gallery</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix">
    
    <div class="masonry-grid row">
								 <?php
    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'gallery'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
    ?>
								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
										<?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full-size');
          ?>
											<img src="<?php echo $image[0];?>" alt="<?php the_title();?>">
											<?php } ?>
											<div class="overlay">
												<div class="overlay-links">
													<?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full-size');
          ?>
													<a href="<?php echo $image[0];?>" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
															<?php } ?>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->
							<?php   
          endwhile; 
           wp_reset_query(); 
  ?>

							</div>
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php get_footer();?>
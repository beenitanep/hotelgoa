<!doctype html>
<html lang="en">
<head>
<title><?php wp_title(':', true, 'right'); ?></title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--Fonts-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
<?php wp_head(); ?>
</head>
<body>
<!--Header Start-->
<header id="header"> <a href="#" class="switchMenu" style="display:none"><i class="fa fa-list"></i></a>
  <div class="innerHeader">
    <div class="row no-gutters">
      <div class="col-lg-4">
        <ul class="top_social_links">
          <li><a href="#" class="facebook" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#" class="twitter" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#" class="instagram" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
      <div class="col-lg-4">
        <h1 class="logo"><a href="<?php bloginfo('url');?>">Hotel Goa <span>Palace Pvt. Ltd.</span></a></h1>
      </div>
      <div class="col-lg-4 topContact">
        <h3><i class="flaticon-telephone"></i><a href="#">977-9813039589</a></h3>
      </div>
    </div>
  </div>
</header>
<nav id="NavMenu" class="navbar navbar-expand-lg navbar-light sps sps--abv"> <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <?php
            wp_nav_menu(array(
                'menu'            => 'primary',
                'theme_location'  => 'primary',
                'container'       => 'div',
                'container_id'    => 'navbarSupportedContent"',
                'container_class' => 'collapse navbar-collapse',
                'menu_id'         => false,
                'menu_class'      => 'navbar-nav',
                'depth'           => 0,
                'fallback_cb'     => 'bs4navwalker::fallback',
                'walker'          => new bs4navwalker()
              ));
        ?>
  </nav>
<!--Header End--> 
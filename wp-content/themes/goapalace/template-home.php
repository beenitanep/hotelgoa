<?php 
/*
Template Name: Home
*/
get_header();?> 
<!--Hero Section Start-->
<section class="heroSection">
  
  <div class="row no-gutters">
    <div class="col-lg-6">
      <div class="sliderBanner">
        <div class="owl-carousel owl-theme">
        <?php
    $args = array(
      'posts_per_page' => 8,
      'post_type' => 'slider'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
    ?>
     <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'slider');
          ?>
          <div class="item"> <img src="<?php echo $image[0];?>" alt="<?php the_title();?>"> </div>
           <?php  
              }
                   endwhile; 
  wp_reset_query(); 
  ?>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="heroSection_right_block">
        <div class="row no-gutters">
          <div class="col-lg-6">
           <?php
        $args = array(
          'post_type' => 'page',
          'p'=>14
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
            <figure> 
            <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'home-trek');
          ?>
            <img src="<?php echo $image[0];?>" alt="<?php the_title();?>">
            <?php } ?>
              <figcaption> <a href="<?php the_permalink();?>" class="btn btn-outline-secondary"><?php the_title();?></a> </figcaption>
            </figure>
            <?php  
        endwhile; 
        wp_reset_query(); 
        ?>
          </div>
          <div class="col-lg-6">
            <div class="blockSection">
            <?php
        $args = array(
          'post_type' => 'page',
          'p'=>21
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
              <h4><?php the_title();?></h4>
              <p><?php echo substr(strip_tags(the_content()),0,50);?></p>
              <a href="<?php the_permalink();?>" class="btn btn-outline-warning">Read More</a> 
              <?php  
        endwhile; 
        wp_reset_query(); 
        ?>
              </div>
          </div>
          <div class="col-lg-6">
          <?php
        $args = array(
          'post_type' => 'page',
          'p'=>23
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
            <figure>
              <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'home-trek');
          ?>
             <img src="<?php echo $image[0];?>" alt="Map">
          <?php } ?>
              <figcaption> <a href="<?php the_permalink();?>" class="btn btn-outline-secondary"><?php the_title();?></a> </figcaption>
            </figure>
            <?php  
        endwhile; 
        wp_reset_query(); 
        ?>
          </div>
          <div class="col-lg-6">
          <?php
        $args = array(
          'post_type' => 'page',
          'p'=>70
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
            <figure> 
            <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'home-trek');
          ?>
            <img src="<?php echo $image[0];?>" alt="<?php the_title();?>">
             <?php } ?>
              <figcaption> <a href="<?php the_permalink();?>" class="btn btn-outline-secondary"><?php the_title();?></a> </figcaption>
            </figure>
            <?php  
        endwhile; 
        wp_reset_query(); 
        ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Hero Section End--> 
<!--Booking Section Start-->
<section class="bookingSection">
 <form method="post" action="<?php bloginfo('template_url');?>/booking.php" class="book-form">
  <div class="row filter_room no-gutters">
    <div class="col-lg-2">
      <h2 class="bookRoom">Book Your <span>Rooms</span></h2>
    </div>
    <div class="col-lg-9">
    
      <div class="row">
        <div class="col-lg-3">
          <h4>Room Category</h4>
          <!-- Build your select: -->
          <select id="roomCategory" multiple="multiple" name= "room_type">
          <?php
    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'room-and-rates'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
    $post_id = get_the_ID();
    ?>
            <option value="<?php the_title();?>"><?php the_title();?></option>
    <?php  
          endwhile; 
           wp_reset_query(); 
  ?>
          </select>
        </div>
        <div class="col-lg-2">
          <h4>Adults</h4>
          <!-- Build your select: -->
          <select id="adults" name= "no-of-adult">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div>
        <div class="col-lg-2">
          <h4>Children</h4>
          <!-- Build your select: -->
          <select id="children" name= "no-of-child">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div>
        <div class="col-lg-5"> <span id="two-inputs"><span class="input-wrap">
          <h4>Check In</h4>
          <input id="date-range200" class="form-control" size="20" value="" name= "checkin-date">
          </span><span class="input-wrap">
          <h4>Check Out</h4>
          <input id="date-range201" class="form-control" size="20" value="" name= "checkout-date">
          </span></span> </div>
      </div>
    </div>
    <div class="col-lg-1">
      <div class="btn-wrapper">
        <button type="submit" class="btn btn-dark">Filter</button>
      </div>
    </div>
  </div>
  </form>
</section>
<!--Booking Section End--> 
<!--About Section Start-->
<section class="aboutSection">
  <div class="container">
    <div class="row">
    <?php
        $args = array(
          'post_type' => 'page',
          'p'=>21
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
      <div class="col-lg-6"> 
<?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'home-about');
          ?>
      <img src="<?php echo $image[0];?>" alt="<?php the_title();?>">
      <?php } ?>
       </div>
      <div class="col-lg-6">
        <div class="introBlock">
          <div class="introContent">
            <h3><?php the_excerpt();?></h3>
            <p><?php echo substr(strip_tags(the_content()),0,50);?></p>
            <a href="<?php the_permalink(); ?>" class="btn btn-outline-warning">Read More</a> </div>
        </div>
      </div>
      <?php  
                   endwhile; 
  wp_reset_query(); 
  ?>
    </div>
  </div>
</section>
<!--About Section End--> 
<!--Service Section Start-->
<section class="serviceSection graySection">
  <div class="container">
    <div class="title text-center">
      <h2>Our Services</h2>
      <p> <span><?php echo get_the_excerpt('59');?></span></p>
    </div>
    <div class="serviceList clearfix">
      <ul>
      <?php
    $args = array(
      'posts_per_page' => 8,
      'post_type' => 'service'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
    ?>
        <li class="clearfix">
          <div class="serviceInfo">
          <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'home-service');
          ?>
          <i class="icon"><img src="<?php echo $image[0];?>" alt="<?php the_title();?>"></i>
          <?php }?>
            <div class="serviceContent">
              <h5><?php the_title();?></h5>
              <p><?php echo substr(strip_tags(get_the_excerpt()),0,130);?></p>
            </div>
          </div>
        </li>
        <?php  
                   endwhile; 
  wp_reset_query(); 
  ?>
      </ul>
    </div>
  </div>
</section>

<!--Room & Rates Start-->
<section class="roomRate">
  <div class="container">
    <div class="title text-center">
      <h2>Room & Rate</h2>
      <p> <span><?php echo get_the_excerpt('62');?></span></p>
    </div>
    <div class="service_block">
      <ul class="nav nav-tabs" role="tablist">
      <?php
    $args = array(
      'posts_per_page' => 3,
      'post_type' => 'room-and-rates'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    $post_slug=$post->post_name;
    while (have_posts()) : the_post();
    $counter++;
    ?>
        <li class="nav-item"> <a class="nav-link <?php echo ($counter==1?'active':'');?>" href="#<?php echo $post->post_name;?>" role="tab" data-toggle="tab"><?php the_title();?></a> </li>
        <?php  
  endwhile; 
  wp_reset_query(); 
  ?>
      </ul>
      
      <!-- Tab panes -->
      <div class="tab-content">
      <?php
    $args = array(
      'posts_per_page' => 3,
      'post_type' => 'room-and-rates'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    global $post;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
    ?>
        <div role="tabpanel" class="tab-pane <?php echo ($counter==1?'slide in active':'fade');?>" id="<?php echo $post->post_name;?>">
          <div class="row">
          <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'home-room');
          ?>
            <div class="col-lg-6"> <img src="<?php echo $image[0];?>" alt="<?php the_title();?>"> </div>
            <?php }?>
            <div class="col-lg-6">
              <div class="roomTitle clearfix">
                <h3><?php the_title();?></h3>
                <a href="#" class="btn btn-outline-warning">Book Now</a> </div>
              <p><?php echo substr(strip_tags(get_the_content()),0,342);?></p>
              <?php 
                   $amenities = get_post_meta( $post->ID, 'amenities', true );
             if(  $amenities!= ''){?>
              <b>Amenities</b>
              <?php } 
               echo $amenities;
                $price = get_post_meta( $post->ID, 'price', true );
             if(  $price!= ''){
              ?>
              <p class="price">Price start at: 
              <?php } ?>
              <b><?php echo $price; ?></b> 
              <?php if(  $price!= ''){ ?>
              for night
              <?php }?>
              </p>
            </div>
          </div>
        </div>
        <?php  
                   endwhile; 
  wp_reset_query(); 
  ?> 
      </div>
    </div>
  </div>
</section>
<!--Room & Rates End--> 
<!--Testimonial Start-->
<section class="testimonial-wrapper parallax">
  <div class="container">
  <div class="title text-center whiteText">
      <h2>Testimonal</h2>
     
    </div>
    <div class="row">
      <div class="col-lg-8 offset-lg-2">
        <div class="owl-carousel owl-theme testimonial">
        <?php
    $args = array(
      'posts_per_page' => 3,
      'post_type' => 'testimonial'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    $post_slug=$post->post_name;
    while (have_posts()) : the_post();
    $counter++;
    ?>
          <div class="item">
            <div class="testimonialInfo">
              <h4><?php echo get_post_meta( $post->ID, 'description', true );?></h4>
              <p><?php echo substr(strip_tags(get_the_content()),0,240);?></p>
              <div class="testimonailDetail">
                <div class="testimonialImg"></div>
                <span class="name"><?php the_title();?></span> <span class="post"><?php echo get_post_meta( $post->ID, 'country', true );?></span> </div>
            </div>
          </div>
           <?php  
                   endwhile; 
  wp_reset_query(); 
  ?> 
        </div>
      </div>
    </div>
  </div>
</section>
<!--Testimonial End--> 
<div class="top-footer">
    	<div class="container custom-container clearfix">
        	<h3 class="float-left">Follow us through social media</h3>
            <ul class="float-right social_media">
            	<li><a href="#"><i class="fa fa-facebook"></i><span><b>3,060</b>Followers</span></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i><span><b>1,060</b>Followers</span></a></li>
                <li><a href="#"><i class="fa fa-tripadvisor"></i><span><b>2,060</b>Followers</span></a></li>
            </ul>
        </div>
    </div>
<?php get_footer();?>
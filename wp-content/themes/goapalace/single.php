<?php get_header(); ?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3><?php the_title();?></h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page"><?php the_title();?></li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix"> 
  <?php
    if (have_posts()) : while (have_posts()) : the_post();
    if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full-size');
    ?>
    <div class="largeImg"><img src="<?php echo $image[0];?>" alt="<?php the_title();?>"> </div>
    <?php }
    the_content();?>
    <?php 
    endwhile; 
endif; ?>
  </div>
</section>
<?php get_footer(); ?>
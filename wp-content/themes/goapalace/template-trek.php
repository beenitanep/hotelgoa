<?php 
/*
Template Name: Trek
*/
get_header();?> 
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Treks</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Treks</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container">
     <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
  <?php $count_snap = 1;
                      $args = array('order' => 'ASC', 'orderby'=> 'menu_order', 'post_type'=> 'attachment', 'post_parent'=> get_the_ID(), 'post_mime_type' => 'image', 'post_status'=> null, 'numberposts'=> -1, 'exclude' => get_post_thumbnail_id());
                      $snap_attachments = get_posts($args);
                      if ($snap_attachments) {
                        
                        foreach ($snap_attachments as $snap_attachment) { 
                        //$img_title = $attachment->post_title;
                        $img_caption = $snap_attachment->post_excerpt;
                        $image_attributes_full = wp_get_attachment_image_src($snap_attachment->ID, 'full size');
                        $image_attributes =wp_get_attachment_image_src( $snap_attachment->ID, 'medium');
          ?>
    <div class="carousel-item <?php echo ($count_snap== 1 ? 'active':'');?>">
      <img class="d-block img-fluid" src="<?php echo $image_attributes_full[0];?>" alt="<?php echo $img_caption;?>">
    </div>
    <?php $count_snap++;  }
          }   ?>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
          
          <div class="btn-wrap clearfix"> <a href="<?php echo get_permalink('23');?>" class="enquirebtn">Enquiry Us</a> <a href="booking-form.php" class="bookbtn">Book This Trip</a> </div>
          
          <!-- tabs start -->
          <div class="tabs-style treks_detail"> 
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li  class="nav-item"><a href="#h2tab1" class="nav-link active" role="tab" data-toggle="tab">Trip Overview</a></li>
              <li class="nav-item"><a href="#h2tab2" class="nav-link" role="tab" data-toggle="tab">Detail Itenary</a></li>
              <li class="nav-item"><a href="#h2tab3" class="nav-link" role="tab" data-toggle="tab">Inclusions & Exclusions</a></li>
              <li class="nav-item"><a href="#h2tab4" class="nav-link" role="tab" data-toggle="tab">Trip Facts</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="h2tab1">
                <p><?php echo get_post_meta( $post->ID, 'overview', true );?></p>
                
              </div>
              <div class="tab-pane fade" id="h2tab2">
                <?php echo get_post_meta( $post->ID, 'itenary', true );?>
              </div>
              <div class="tab-pane fade" id="h2tab3"> <strong><?php echo get_post_meta( $post->ID, 'tripinfo', true );?></strong>
                <div class="row">
                  <div class="col-md-6">
                    <h3>Includes:</h3>
                    <?php echo get_post_meta( $post->ID, 'included', true );?>
                  </div>
                  <div class="col-md-6">
                    <h3>Excludes:</h3>
                    <?php echo get_post_meta( $post->ID, 'excluded', true );?>
                  </div>
                </div>
                </div>
                <div class="tab-pane fade" id="h2tab4">
                <div class="facts-module">
                
                  <div class="box">
                    <ul>
  <li>
    <span class="dt">country</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'tripcountry', true );?></span>
  </li>
  <li>
    <span class="dt">Destination</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'tripdestination', true );?></span>
  </li>
  <li>
    <span class="dt">Duration</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'tripduration', true );?></span>
  </li>
  <li>
    <span class="dt">Activities</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'tripactivity', true );?></span>
  </li>
  <li>
    <span class="dt">Grade</span>
    <span class="dd">
      <span class="text"><?php echo get_post_meta( $post->ID, 'tripgrade', true );?><a href="#" data-toggle="modal" data-target="#myModal">Learn</a>
            <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
      <?php echo get_post_meta( $post->ID, 'tripgrademore', true );?>  
    </div>
  </div>
</div>
            </span>
    </span>
  </li>
  <li>
    <span class="dt">Best Season</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'bestseason', true );?></span>
  </li>
  <li>
    <span class="dt">Group Size</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'groupsize', true );?></span>
  </li>
  <li>
    <span class="dt">Altitude</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'altitude', true );?></span>
  </li>
  <li>
    <span class="dt">Starts/Ends</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'startend', true );?></span>
  </li>
  <li>
    <span class="dt">Accommodation</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'accommodation', true );?></span>
  </li>
  <li>
    <span class="dt">Meals included</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'meal', true );?></span>
  </li>
  <li>
    <span class="dt">Transportation</span>
    <span class="dd"><?php echo get_post_meta( $post->ID, 'transportation', true );?></span>
  </li>
</ul>                 </div>
                </div>
                
                </div>
            </div>
          </div>
          <!-- tabs end --> 
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php get_footer();?>
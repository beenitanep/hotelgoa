<?php 
/*
Template Name: Room
*/
get_header();?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Room & Rates</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Room & Rates</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container">
  	<div class="row">
<?php
    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'room-and-rates'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
    ?>
    	<div class="col-lg-4">
        	<div class="roomList">
            <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'home-room-and-rate');
          ?>
            	<div class="roomImg"><a href="<?php the_permalink();?>"><img src="<?php echo $image[0];?>" alt="<?php the_title();?>"></a></div>
                <?php }?>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                    <?php 
                   $price = get_post_meta( $post->ID, 'price', true );?>
					<span class="price"><?php echo $price; if(  $price!= ''){?>/<sub>Night</sub><?php } ?></span>
					</div>
                    <p><?php echo substr(strip_tags(get_the_excerpt()),0,73);?>...</p>
                    <a href="<?php bloginfo('template_url');?>/booking.php" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
   <?php   
          endwhile; 
           wp_reset_query(); 
  ?>    
    </div>
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php get_footer();?>
<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Mardi Himal
 * @since Mardi Himal 1.0
 */
get_header(); ?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3><?php the_title();?></h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Room Detail</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container">
    <div class="roomDetail">
    <div class="row">
    <?php
    if (have_posts()) : while (have_posts()) : the_post();
      $custom = get_post_custom();
       if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'room-details');
    ?>
    	<div class="col-lg-6">
        	<img src="<?php echo $image[0];?>" alt="<?php the_title();?>">  
        </div>
         <?php }?>
        <div class="col-lg-6">
        	<div class="detail_title"><h3><?php the_title();?></h3>
        <p class="lead"><?php echo get_post_meta( $post->ID, 'description', true );?></p>
        <?php 
        $price = get_post_meta( $post->ID, 'price', true );
        ?>
        	<h5><?php if(  $price!= ''){?> Prices start at:<?php } ?> <b><?php echo $price; ?></b> <?php if(  $price!= ''){?> /Night <?php } ?></h5>
        </div>
        <?php the_content();?>
        <?php 
                   $amenities = get_post_meta( $post->ID, 'amenities', true );
             if(  $amenities!= ''){?>
        <hr/>
        <h5>Amenities</h5>
        <?php } 
        echo $amenities;
        ?>
        <div class="btn-wrapper"><a href="<?php bloginfo('template_url');?>/booking.php" class="btn btn-outline-warning">Book Now</a></div>
        </div>
        <?php       
        endwhile; endif;     
        ?> 
    </div>
    	
    </div>
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php get_footer(); ?>